using System;
interface IPaintable {
    void Paint();
}

abstract class Figure {
    public abstract int GetDimension ();
    public abstract string GetFigureType();
}

class Point : Figure, IPaintable
{
  private string name;
  private double x;
  private double? y = null;
  private double? z = null;
  private int dimension;
  public Point (string name, double x)
  {
    this.name = name;
    this.x = x;
    this.dimension = 1;
  }
  public Point (string name, double x, double y) {
      this.name = name;
      this.x = x;
      this.y = y;
      this.dimension = 2;
  } 
  public Point (string name, double x, double y, double z) {
      this.name = name;
      this.x = x;
      this.y = y;
      this.z = z;
      this.dimension = 3;
  } 
  public string Name
  {
      get {
            return name;
      }
  }
  public int Dimension {
      get {
          return dimension; 
      }
  }
  public override int GetDimension() {
      return this.dimension;
  }
  public override string GetFigureType() {
      return "Point";
  }
  public void printPoint ()
  {
    Console.WriteLine (name);
  }
  void IPaintable.Paint() {
      switch (this.dimension) {
          case 1: 
            Console.WriteLine("Координата точки "+ this.name + ": " + this.x);
            break;
          case 2: 
            Console.WriteLine("Координаты точки "+ this.name + ": " + this.x + ", " + this.y);
            break;
          case 3: 
            Console.WriteLine("Координаты точки "+ this.name + ": " + this.x + ", " + this.y + ", " + this.z);
            break;
          default:
            break;
      }
      if (this.dimension == 1) {
          
      }
      
  }
}


class TestPoint
{
  static string GetNameWithCoordinates(Point point){
    return ("Точка "+ point.Name + ": количество координат: " + point.Dimension);
    }
    
    public static void Main (string[]args){
        IPaintable p0 = new Point ("abc", 12.4, 9, 99);
        p0.Paint();
        Point p1 = new Point ("a", 12.4);
        Console.WriteLine(GetNameWithCoordinates(p1));
        Point p2 = new Point ("b", 12.4, 9.75);
        Console.WriteLine(GetNameWithCoordinates(p2));
        Point p3 = new Point ("c", 12.4, 13, 15.1);
        Console.WriteLine(GetNameWithCoordinates(p3));
        Console.WriteLine(p2.GetFigureType());
        Console.WriteLine(p3.GetDimension());
    }
}

